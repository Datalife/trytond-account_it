===================
Account It Scenario
===================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install account_it::

    >>> config = activate_modules('account_it')
